package com.noobsky;

import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by kamil on 03.03.17.
 */
public class SaveController implements Initializable {

    private Controller controller;

    private int originalWidth, originalHeight;

    /////////////////

    @FXML
    private Label path;
    private File file = null;

    @FXML
    private TextField widthField;
    @FXML
    private TextField heightField;


    @FXML
    private void cancel() {
        ((Stage) path.getScene().getWindow()).close();
    }

    @FXML
    private void save() {
        if (file != null) {
            try {
                int w = Integer.parseInt(widthField.getText());
                int h = Integer.parseInt(heightField.getText());

                Mat img = controller.getPreviewImageMat();

                if (w != originalWidth || h != originalHeight) {
                    Mat tmp = new Mat();
                    Imgproc.resize(img, tmp, new Size(w,h), 0, 0, Imgproc.INTER_AREA);

                    img = tmp;
                }

                if (!file.getName().endsWith(".gif"))
                    Imgcodecs.imwrite(file.getAbsolutePath(), img);
                else {
                    Image image = ImgUtil.matToImage(img);
                    try {
                        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "GIF", file);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                }
            }
            catch (NumberFormatException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Not a number");
                alert.setContentText("Enter correct numbers in width and height");
                alert.show();
                return;
            }
        }
        ((Stage) path.getScene().getWindow()).close();
    }

    @FXML
    private void restore() {
        widthField.setText(originalWidth + "");
        heightField.setText(originalHeight + "");
    }

    @FXML
    private void selectFile() {
        FileChooser fileChooser = new FileChooser();
        ObservableList<FileChooser.ExtensionFilter> extensionFilters =  fileChooser.getExtensionFilters();

        extensionFilters.add(new FileChooser.ExtensionFilter("PNG originalImageMat", "*.png"));
        extensionFilters.add(new FileChooser.ExtensionFilter("JPEG originalImageMat", "*.jpg"));
        extensionFilters.add(new FileChooser.ExtensionFilter("TIFF originalImageMat", "*.tiff"));
        extensionFilters.add(new FileChooser.ExtensionFilter("GIF originalImageMat", "*.gif"));
        extensionFilters.add(new FileChooser.ExtensionFilter("Bitmap originalImageMat", "*.bmp"));

        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            String extension = fileChooser.getSelectedExtensionFilter().getExtensions().get(0);
            extension = extension.substring(1);

            if (!file.getName().endsWith(extension)) {
                file = new File(file.getAbsolutePath() + extension);
            }

            this.file = file;

            path.setText(file.getAbsolutePath());
        }

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        selectFile();
        controller = Controller.getInstance();
        Mat mat = controller.getPreviewImageMat();

        originalWidth = mat.width();
        originalHeight = mat.height();

        restore();
    }
}
