package com.noobsky;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Transform;

public class ZoomingPane extends Pane {
    private DoubleProperty zoomFactor = new SimpleDoubleProperty(1);
    private static final double MIN_ZOOM = 0.1;
    private static final double MAX_ZOOM = 200.0;
    private double baseWidth = -1, baseHeight = -1;

    public ZoomingPane() {
        getChildren().addListener(new ListChangeListener<Node>() {

            @Override
            public void onChanged(javafx.collections.ListChangeListener.Change<? extends Node> c) {
                while (c.next()) {
                    if (c.getAddedSize() > 0) {
                        for (Node n : c.getAddedSubList()) {

                            Scale scale = getNodesScaleTransform(n);

                            zoomFactor.addListener(new ChangeListener<Number>() {
                                public void changed(ObservableValue<? extends Number> observable, Number oldValue,
                                                    Number newValue) {
                                    scale.setX(newValue.doubleValue());
                                    scale.setY(newValue.doubleValue());
                                    requestLayout();
                                }
                            });
                        }
                    }
                }
            }

        });
    }

    private static final Scale getNodesScaleTransform(Node n) {
        for (Transform t : n.getTransforms()) {
            if (t instanceof Scale) {
                return (Scale) t;
            }
        }
        Scale s = new Scale(1, 1);
        n.getTransforms().add(s);
        return s;
    }

    protected void layoutChildren() {
        Pos pos = Pos.TOP_LEFT;
        double width = getWidth();
        double height = getHeight();
        double top = getInsets().getTop();
        double right = getInsets().getRight();
        double left = getInsets().getLeft();
        double bottom = getInsets().getBottom();
        double contentWidth = (width - left - right) / zoomFactor.get();
        double contentHeight = (height - top - bottom) / zoomFactor.get();
        for (Node n : getChildren()) {
            layoutInArea(n, left, top, contentWidth, contentHeight, 0, null, pos.getHpos(), pos.getVpos());
        }
    }

    public final Double getZoomFactor() {
        return zoomFactor.get();
    }

    public final void setZoomFactor(Double zoomFactor) {
        if (zoomFactor < MIN_ZOOM) {
            zoomFactor = MIN_ZOOM;
        } else if (zoomFactor > MAX_ZOOM) {
            zoomFactor = MAX_ZOOM;
        }
        if (baseWidth < 0) {
            baseWidth = getWidth();
        }
        if (baseHeight < 0) {
            baseHeight = getHeight();
        }
        //System.out.println(baseWidth + " " + baseHeight);
        this.zoomFactor.set(zoomFactor);
        setPrefHeight(baseHeight * zoomFactor);
        setPrefWidth(baseWidth * zoomFactor);
        requestLayout();
    }

    public final void resetBaseSize() {
        baseWidth = -1;
        baseHeight = -1;
    }

    public final void setBaseSize(double baseWidth, double baseHeight) {
        this.baseWidth = baseWidth;
        this.baseHeight = baseHeight;
    }

    public final DoubleProperty zoomFactorProperty() {
        return zoomFactor;
    }
}